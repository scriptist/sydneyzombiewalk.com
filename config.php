<?php

date_default_timezone_set('Australia/Sydney');

// Past, present, and future walk details
$currentWalk = '2016';
$walks = array(
	'2012' => array(
		'meetTime'  => strtotime('2012-10-27 15:30:00'),
	),
	'2013' => array(
		'meetTime'  => strtotime('2013-11-02 15:30:00'),
	),
	'2014' => array(
		'meetTime'  => strtotime('2014-11-01 15:00:00'),
	),
	'2015' => array(
		'meetTime'  => strtotime('2015-10-31 15:00:00'),
		'startTime' => strtotime('2015-10-31 16:00:00'),
		'endTime'   => strtotime('2015-10-31 18:00:00'),
	),
	'2016' => array(
		'makeupCloseTime' => strtotime('2016-10-29 00:00:00'),
		'meetTime'  => strtotime('2016-10-29 15:00:00'),
		'startTime' => strtotime('2016-10-29 16:00:00'),
		'endTime'   => strtotime('2016-10-29 18:00:00'),
	),
);

// Add $walks['current']
$walks[$currentWalk]['year'] = $currentWalk;
$walks['current'] = $walks[$currentWalk];

// Build config object
global $config;
$config = array(
	'cacheBreakAssets' => true,
	'makeupEnabled' => true && time() < $walks['current']['makeupCloseTime'],
	'develop'       => false,
	'walks'         => $walks,
	'postWalk'      => time() > $walks['current']['endTime'],
);

?>
