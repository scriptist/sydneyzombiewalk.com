<?php

$app->get("/fundraising", function () use ($app) {
	$app->render("fundraising/index.html.twig");
})->setName('fundraising');

$app->map("/fundraising/profile/:id(/(:name))", function ($id, $name = "") use ($app) {
	global $config;

	// Cache data so it does not need to be fetched again on redirect
	$cache_path = 'app/cache/fundraising/';
	$cache_id = ($config['develop'] ? 'develop-' : '') . $id;
	if (!file_exists($cache_path))
		mkdir($cache_path, 0777, true);
	$cache = new Gilbitron\Util\SimpleCache();
	$cache->cache_path = $cache_path;
	$cache->cache_time = 20 * 60;

	if (!($json = $cache->get_cache($cache_id))) {
		if ($config['develop']) {
			$apiUrl = "http://localhost.api.sydneyzombiewalk.com/v1";
		} else {
			$apiUrl = "http://api.sydneyzombiewalk.com/v1";
		}

		$context = stream_context_create(array(
			'http' => array('ignore_errors' => true)
		));

		try {
			$json = file_get_contents("$apiUrl/fundraising/profile/$id", false, $context);
		} catch (Exception $e) {
			return $app->render("fundraising/profile.html.twig", array('profile' => null));
		}
		if (!json_decode($json)) {
			$app->pass();
		}
		$cache->set_cache($cache_id, $json);
	}

	$obj = json_decode($json);

	// No profile with this ID
	if ($obj->error)
		$app->pass();

	$slugify = new Cocur\Slugify\Slugify();
	$correctName = $slugify->slugify($obj->profile->name);

	if ($name == $correctName) // Render the profile
		$app->render("fundraising/profile.html.twig", array('profile' => $obj->profile));
	else // Redirect to the correct name
		$app->redirect("/fundraising/profile/$id/$correctName");
})->via('GET', 'POST')->setName('fundraising-profile');

$app->get("/fp/:id(/:otherstuff+)", function ($id, $otherstuff = null) use ($app) {
	$app->redirect($app->urlFor('fundraising-profile', array('id'=>$id)));
});

?>