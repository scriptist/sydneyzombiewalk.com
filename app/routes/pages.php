<?php

$pages = array(
	'about',
	'contact',
	'makeup',
	'makeup-booking',
	'makeup-booking/success',
	'photographers',
	'press',
	'privacy',
	'rules',
	'volunteer',
	'wranglers',
);

foreach ($pages as $page) {
	$url = '/' . $page;
	$app->map($url, function () use ($app, $page) {
		$filename = str_replace('/', '-', $page);
		$app->render("pages/$filename.html.twig");
	})->via('GET', 'POST')->setName($page);
}

?>
