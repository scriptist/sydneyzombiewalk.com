<?php

$app->get("/ticket/:key", function ($key) use ($app) {
	$app->render("pages/ticket.html.twig", array('key' => $key));
})->setName('ticket');

?>