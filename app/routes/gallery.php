<?php

$app->get("/gallery", function () use ($app) {

	$cache_path = 'app/cache/gallery/';
	if (!file_exists($cache_path))
		mkdir($cache_path, 0777, true);
	$cache = new Gilbitron\Util\SimpleCache();
	$cache->cache_path = $cache_path;

	if ($data = $cache->get_cache('images')) {
		$images = json_decode($data);
	} else {
		try {
			$api = new \Cloudinary\Api();
			$result = $api->resources(array("type" => "upload", "prefix" => "gallery/2015", "max_results" => "500"));
			$images = $result['resources'];
			$cache->set_cache('images', json_encode($images));
		} catch (Exception $e) {
			$images = null;
		}
	}

	$app->render("pages/gallery.html.twig", array('images' => $images));
})->setName('gallery');

?>