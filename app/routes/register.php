<?php

$app->get("/register", function () use ($app) {
	global $config;
	$beginningOfWalkDay = strtotime("midnight", $config['walks']['current']['meetTime']);
	$otd = time() > $beginningOfWalkDay && time() < $config['walks']['current']['endTime'];
	$app->render("pages/register.html.twig", array('otd' => $otd));
})->setName('register');

$app->get("/register(/otd)", function () use ($app) {
	$app->render("pages/register.html.twig", array('otd' => true));
})->setName('register_otd');

$app->get("/reg", function () use ($app) {
	$app->redirect('/register/otd');
});

?>