'use strict';

const key = 'farewell-message';

const text = `
<div class="farewell-container">
<div class="farewell">
<h1><a id="To_all_of_our_wonderful_fans_0"></a>To all of our wonderful fans</h1>
<p>I’m <a href="http://scriptist.io" style="color: inherit; text-decoration: inherit;">Michael Berman</a>, the founder of the Sydney Zombie Walk. I started the walk back when I was still at uni in 2009, and last year I ran my tenth walk! If you’ve been to any of our past events, you may remember seeing me holding a megaphone trying to should loudly enough for the horde of thousands of zombies to hear me.</p>
<p><img src="https://res.cloudinary.com/szw/image/upload/structure/michael.jpg" alt="Me at Sydney Zombie Walk 2011"></p>
<p>Running the zombie walk has been the highlight of my life over the past eight years. Seeing all of you get together just to be a little silly in the middle of the city has brought endless joy to my heart.</p>
<p>Unfortunately, due to a disagreement with a third party, and I am unable to continue running the walk.</p>
<p>The website for future zombie walks is <a href="http://sydneyzombiewalk.com.au" rel="nofollow">here</a>. You will no longer be on the mailing list when this website is shut down so please sign up at <a href="http://sydneyzombiewalk.com.au" rel="nofollow">here</a>. if you wish to continue to be kept up to date on the Sydney event.</p>
<p>I’d also like to put in a good word for the Brain Foundation. We’ve donated tens of thousands of dollars to them over the years, and they’re a very worthy cause. If anybody would like to continue supporting research into brain disorders, <a href="http://brainfoundation.org.au/donation/">please make a donation on their website</a>.</p>
<p>Lastly, I’d like to thank everybody who’s helped out with the Sydney Zombie Walk over the years. This includes The Brain Foundation, NSW Police, all our wranglers and team leaders, and make-up artists, and our loyal fans. This has been an incredible journey and it wouldn’t have been possible without you.</p>
<p>If anybody would like to contact me, just shoot an email through to <a href="mailto:michael@sydneyzombiewalk.com">michael@sydneyzombiewalk.com</a> - I’ll be keeping that email address alive at least for the next couple of months.</p>
<p>Best of luck,<br>
Michael Berman</p>
</div>
<style>
.farewell-container {
	background: rgba(0, 0, 0, 0.7);
	display: flex;
	align-items: center;
	justify-content: center;
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 1000000;
}
.farewell {
	overflow: auto;
	max-width: 90%;
	width: 600px;
	padding: 20px;
	background: white;
	max-height: 90%;
	position: relative;
}
.farewell .close {
	position: absolute;
	top: 10px;
	right: 10px;
}
.farewell h1 {
	margin-top: 5px;
	margin-right: 50px;
	font-family: Roboto, sans-serif;
	font-size: 28px;
	text-transform: none;
}
</style>
</div>
`;

class Farewell {
	constructor() {
		if (!window.localStorage || !localStorage[key]) {
			this.openFarewell();
		}
	}

	openFarewell() {
		this.elm = document.createElement('div');
		this.elm.innerHTML = text;
		const closeElm = document.createElement('button');
		closeElm.addEventListener('click', this.closeFarewell.bind(this));
		closeElm.className = 'close';
		closeElm.innerHTML = 'Close';
		const closeParent = this.elm.querySelector('.farewell');
		closeParent.insertBefore(closeElm, closeParent.firstChild);

		document.body.appendChild(this.elm);
	}

	closeFarewell() {
		if (window.localStorage) {
			localStorage[key] = '1';
		}
		if (this.elm) {
			this.elm.parentNode.removeChild(this.elm);
			this.elm = null;
		}
	}

}

new Farewell();
