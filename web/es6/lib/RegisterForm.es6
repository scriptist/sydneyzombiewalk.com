'use strict';

const Handlebars = require('Handlebars');
const facebookLoader = require('lib/facebookLoader.es6')();
const ZombieAPI = require('lib/ZombieAPI.es6');

module.exports = class RegisterForm {
	constructor(form) {
		this.facebookLoader = facebookLoader;
		this.form = form;
		this.api = new ZombieAPI();
		this.FB = null;

		this.template = {
			success: Handlebars.compile(document.getElementById('template-register-success').innerHTML),
		};

		this.facebookLoader.load((FB) => {
			this.FB = FB;
		});

		let facebookButton = this.form.querySelector('.button--facebook');
		facebookButton.addEventListener('click', this.onFacebookClick.bind(this));
		this.form.addEventListener('submit', this.onSubmit.bind(this));
	}

	onFacebookClick(e) {
		e && e.preventDefault();
		if (!this.FB) {
			this.facebookLoader.load(() => {
				this.onFacebookClick();
			});
		}

		this.form.classList.add('form--is-busy');
		this.FB.getLoginStatus((response) => {
			if (response.status === 'connected') {
				this.getFacebookInfo();
			} else {
				this.FB.login((response) => {
					if (response.status === 'connected') {
						this.getFacebookInfo();
					} else {
						alert('Error: failed to connect to Facebook');
						this.form.classList.remove('form--is-busy');
					}
				}, {
					scope: 'email',
				});
			}
		});
	}

	getFacebookInfo() {
		this.FB.api('/me', {
			fields: 'name,email',
		}, (response) => {
			this.submitDetails(response.name, response.email, 'facebook');
		});
	}

	onSubmit(e) {
		e.preventDefault();
		let name = this.form.querySelector('input[name=name]').value;
		let email = this.form.querySelector('input[name=email]').value;
		this.submitDetails(name, email, 'manual');
	}

	submitDetails(name, email, source) {
		let year = this.form.querySelector('input[name=year]').value;
		this.form.classList.add('form--is-busy');

		this.api.post('/registration/register', {
			name: name,
			email: email,
			year: year,
			source: source,
		}, (response) => {
			this.form.classList.remove('form--is-busy');
			if (!response.error) {
				this.form.querySelector('.form__success').innerHTML = this.template.success(response);
				this.form.classList.add('form--is-success');
			} else {
				alert('Error: ' + response.msg);
			}
		});
	}

};
