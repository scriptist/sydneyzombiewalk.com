'use strict';

const Handlebars = require('Handlebars');

const Modal = require('lib/Modal.es6');
const fundraisingAPI = require('lib/fundraisingAPI.es6')();
const utils = require('lib/utils.es6');

Handlebars.registerHelper('currencyFormat', function (val) {
	return utils.currencyFormat(val);
});
Handlebars.registerHelper('dateFormat', function (val) {
	return utils.dateFormat(val);
});

module.exports = class FundraisingProfile {
	constructor(parent, id) {
		id = parseInt(id);
		if (isNaN(id))
			id = parseInt(parent.getAttribute('data-profile-id'));

		this.parent = parent;
		this.id = id;
		this.api = fundraisingAPI;

		this.template = {
			profile : Handlebars.compile(document.getElementById('template-profile').innerHTML),
			donate  : Handlebars.compile(document.getElementById('template-donate' ).innerHTML),
			edit    : Handlebars.compile(document.getElementById('template-edit'   ).innerHTML),
			paypal  : Handlebars.compile(document.getElementById('template-paypal' ).innerHTML),
			back    : Handlebars.compile(document.getElementById('template-back'   ).innerHTML),
		};

		this.parent.classList.add('profile--is-busy');
		this.api.profile(this.id, this.onLoad.bind(this));
	}

	onLoad(data) {
		if ('profile' in data) {
			this.profile = data.profile;
			this.profile.barHeight = this.getBarHeight();
			this.profile.targetPercent = this.getTargetPercent(true);
		}

		this.render();

		if (location.hash === '#welcome') {
			this.openEditModal(true);
			return location.hash = '';
		} else if (this.profile && parseInt(this.profile.id) === this.api.session.profileId && !('target' in this.profile)) {
			this.openWelcomeModal();
		}
	}

	render() {
		var editable = this.profile && parseInt(this.profile.id) === this.api.session.profileId;
		this.parent.innerHTML = this.template.profile({profile: this.profile, editable: editable});
		this.attachEventListeners();
		return this.parent.classList.remove('profile--is-busy');
	}

	attachEventListeners() {
		var buttons = {
			donate: this.openDonateModal,
			edit  : this.openEditModal,
			logout: this.logout,
		};

		for (const key in buttons) {
			const elms = document.querySelectorAll(`button[data-${key}`);
			const func = buttons[key].bind(this);

			for (var i = 0; i < elms.length; i++) {
				elms[i].addEventListener('click', func);
			}
		}
	}

	openWelcomeModal() {
		var modal = new Modal(this.template.back({profile: this.profile}), ['small']);

		modal.parent.querySelector('button[data-join]').addEventListener('click', () => {
			modal.destroy();
			return this.openEditModal(true);
		});
		return modal.parent.querySelector('button[data-logout]').addEventListener('click', this.logout);
	}

	openDonateModal() {
		var modal = new Modal(this.template.donate({profile: this.profile}));
		var form = modal.parent.querySelector('form');

		form.addEventListener('submit', (e) => {
			e.preventDefault();

			var saveData = {
				'donor_name'   : form.querySelector('[name=donor_name]'   ).value,
				'donor_email'  : form.querySelector('[name=donor_email]'  ).value,
				'amount'       : form.querySelector('[name=amount]'       ).value,
				'donor_comment': form.querySelector('[name=donor_comment]').value,
			};

			form.classList.add('form--is-busy');
			return this.api.registerDonation(this.id, saveData, (data) => {
				if (data.error) {
					form.classList.remove('form--is-busy');
					return alert(`Error: ${data.msg}`);
				} else {
					var html = this.template.paypal({
						'amount'      : data.data.amount,
						'donation_id' : data.donation_id,
						'profile'     : this.profile,
						'href'        : location.href,
						'apiBase'     : `https:${this.api.api.baseURL}`,
					});

					var wrapper = document.createElement('div');
					wrapper.innerHTML = html;
					modal.inner.appendChild(wrapper);

					return wrapper.querySelector('form').submit();
				}
			});
		});


		return form.querySelector('button[type=button]').addEventListener('click', () => {
			return modal.destroy();
		});
	}

	openEditModal(welcome) {
		const modal = new Modal(this.template.edit({
			profile: this.profile,
			welcome: welcome == true,
		}));
		var form = modal.parent.querySelector('form');

		form.addEventListener('submit', (e) => {
			e.preventDefault();

			var saveData = {
				name       : form.querySelector('[name=name]'       ).value,
				target     : form.querySelector('[name=target]'     ).value,
				description: form.querySelector('[name=description]').value,
			};

			form.classList.add('form--is-busy');
			return this.api.editProfile(this.id, saveData, (data) => {
				if (data.error) {
					form.classList.remove('form--is-busy');
					return alert(`Error: ${data.msg}`);
				} else {
					modal.destroy();
					return this.onLoad(data);
				}
			});
		});

		return form.querySelector('button[type=button]').addEventListener('click', function() {
			return modal.destroy();
		});
	}

	logout() {
		return this.api.logout(function() {
			return location.href = '/fundraising';
		});
	}

	getBarHeight() {
		var min = 18.5;
		var max = 96.5;
		var percent = this.getTargetPercent();

		return (percent / 100) * (max - min) + min;
	}

	getTargetPercent(round = false) {
		if (!('id' in this.profile)) {
			return 0;
		}

		var percent = 100 * this.profile.total / this.profile.target;
		if (round) {
			percent = Math.round(percent);
		}

		return Math.min(100, percent);
	}
};
