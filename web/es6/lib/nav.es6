'use strict';

module.exports = () => {
	const navButton = document.querySelector('.nav-button');

	if (navButton) {
		navButton.addEventListener('click', () => {
			return document.documentElement.classList.toggle('is-nav-open');
		});
	}
};
