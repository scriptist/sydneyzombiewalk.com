'use strict';

module.exports = class Modal {
	constructor(html, classes) {
		this.transitionDuration = 200;
		this.isOpen = false;

		this.parent = document.createElement('div');
		this.parent.className = 'modal';

		if (classes) {
			for (var i = 0, c; i < classes.length; i++) {
				c = classes[i];
				this.parent.classList.add(`modal--${c}`);
			}
		}

		this.inner = document.createElement('div');
		this.inner.className = 'modal__inner';
		this.inner.innerHTML = html;

		this.parent.appendChild(this.inner);
		document.body.appendChild(this.parent);

		this.open();
	}

	open(callback) {
		if (this.isOpen) {
			if (typeof callback === 'function')
				callback();
			return;
		}

		this.isOpen = true;
		this.parent.classList.add('modal--transitioning');
		document.documentElement.classList.add('is-modal-open');

		setTimeout(() => {
			this.parent.classList.add('modal--open');

			setTimeout(() => {
				this.parent.classList.remove('modal--transitioning');

				if (typeof callback == 'function')
					callback();
			}, this.transitionDuration);
		}, 50);
	}

	close(callback) {
		if (!this.isOpen) {
			if (typeof callback === 'function')
				callback();
			return;
		}

		this.isOpen = false;
		this.parent.classList.add('modal--transitioning');
		this.parent.classList.remove('modal--open');
		document.documentElement.classList.remove('is-modal-open');

		setTimeout(() => {
			this.parent.classList.remove('modal--transitioning');
			if (typeof callback == 'function')
				callback();
		}, this.transitionDuration);
	}

	destroy() {
		this.close(() => {
			document.body.removeChild(this.parent);
		});
	}

};
