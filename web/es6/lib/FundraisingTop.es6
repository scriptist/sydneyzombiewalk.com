'use strict';

const utils = require('lib/utils.es6');
const fundraisingAPI = require('lib/fundraisingAPI.es6')();

module.exports = class FundraisingTop {
	constructor(parent) {
		this.parent = parent;
		this.api = fundraisingAPI;

		try {
			this.render(JSON.parse(sessionStorage['fundraising-top']));
		} catch (error) {
			this.api.top(4, this.onLoad.bind(this));
			this.parent.classList.add('fundraising-top--is-busy');
		}
	}


	onLoad(data) {
		this.render.call(this, data);
		try {
			return sessionStorage['fundraising-top'] = JSON.stringify(data);
		} catch (error) {
			// Do nothing
		}
	}


	render(data) {
		this.parent.classList.remove('fundraising-top--is-busy');

		if (data.error) {
			return this.parent.innerHTML = '<div class="alert alert--error"><p>Error loading top fundraisers.</p></div>';
		}
		var html = '';
		for (var i = 0, profile; i < data.profiles.length; i++) {
			profile = data.profiles[i];
			var bg = profile.photo ? `${profile.photo}?width=320` : '//res.cloudinary.com/szw/image/upload/structure/fundraising/default.png';

			html += `
				<a href="/fundraising/profile/${profile.id}" class="fundraising-top__profile">
					<div class="fundraising-top__profile__inner" style="background-image:url(${bg});">
						<div class="fundraising-top__profile__info">
							<h3 class="fundraising-top__profile__name">${profile.name}</h3>
							<div class="fundraising-top__profile__amount">Raised ${utils.currencyFormat(profile.total)}</div>
						</div>
					</div>
				</a>
			`;
		}

		if (!data.profiles.length) {
			html += '<p class="fundraising-top__message">Nobody has raised money for the upcoming Sydney Zombie Walk yet.</p>';
		}

		return this.parent.innerHTML = html;
	}
};
