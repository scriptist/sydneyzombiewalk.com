'use strict';

class FacebookLoader {
	constructor(options) {
		this.options = options;
		this.state = 'initialized';
		this.callbacks = [];
		this.FB = null;
	}

	load(callback) {
		if (this.state == 'loaded')
			return typeof callback == 'function' && callback(this.FB);

		if (typeof callback == 'function')
			this.callbacks.push(callback);

		if (this.state == 'loading')
			return;

		this.state = 'loading';
		window.fbAsyncInit = this.onLoad.bind(this);

		// Async loader provided by developers.facebook.com
		(function(d, s, id){
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {
				return;
			}
			js = d.createElement(s); js.id = id;
			js.src = '//connect.facebook.net/en_US/sdk.js';
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

	}

	onLoad() {
		this.FB = window.FB;
		this.state = 'loaded';

		this.FB.init(this.options);
		this.callbacks.forEach((callback) => {
			try {
				callback(this.FB);
			} catch(e) {
				// Do nothing
			}
		});
	}
}

let facebookLoader;
module.exports = () => {
	facebookLoader = facebookLoader || new FacebookLoader({
		appId: '330484023650533',
		xfbml: false,
		version: 'v2.6',
	});

	return facebookLoader;
};
