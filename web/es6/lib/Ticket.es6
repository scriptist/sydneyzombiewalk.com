'use strict';

const Handlebars = require('Handlebars');
const ZombieAPI = require('lib/ZombieAPI.es6');
const utils = require('lib/utils.es6');

Handlebars.registerHelper('dateTimeFormat', function (val) {
	return utils.dateFormat(val, true);
});

module.exports = class Ticket {
	constructor(parent) {
		this.parent = parent;
		this.api = new ZombieAPI();

		this.onLoad = this.onLoad.bind(this);
		this.key = this.parent.getAttribute('data-key');

		this.template = {
			ticket: Handlebars.compile(document.getElementById('template-ticket').innerHTML),
		};

		this.parent.classList.add('ticket--is-busy');
		this.render();
		this.api.get(`/registration/key/${this.key}`, this.onLoad);
	}

	onLoad(data) {
		this.parent.classList.remove('ticket--is-busy');

		return this.render(data);
	}

	render(data) {
		this.parent.innerHTML = this.template.ticket(data);
		const svgs = this.parent.querySelectorAll('svg');
		for (var i = 0; i < svgs.length; i++) {
			const svg = svgs[i];
			svg.setAttribute('viewBox', `0 0 ${svg.getAttribute('width')} ${svg.getAttribute('height')}`);
		}
	}
};
