'use strict';

const facebookLoader = require('lib/facebookLoader.es6')();
const fundraisingAPI = require('lib/fundraisingAPI.es6')();
const Handlebars = require('Handlebars');

module.exports = class FundraisingLogin {
	constructor(parent) {
		this.parent = parent;
		this.api = fundraisingAPI;

		this.template = {
			login: Handlebars.compile(document.getElementById('template-login').innerHTML),
		};

		// Load Facebook API
		this.FB = null;
		facebookLoader.load((FB) => this.FB = FB);

		this.parent.innerHTML = this.template.login({session: this.api.session});

		if (this.api.session.loggedIn) {
			this.parent.querySelector('button[data-logout]')     .addEventListener('click', this.onLogoutClick.bind(this));
		} else {
			this.parent.querySelector('button[data-login]')      .addEventListener('click', this.onLoginClick.bind(this));
			this.parent.querySelector('button[data-register]')   .addEventListener('click', this.onLoginClick.bind(this));
			this.parent.querySelector('button[data-nofacebook]') .addEventListener('click', () => {
				this.parent.classList.add('fundraising-login--nofacebook');
			});
			this.parent.querySelector('form.fundraising-login__nofacebook').addEventListener('submit', this.onLoginSubmit.bind(this));
		}
	}

	onLoginClick(e) {
		e && e.preventDefault();

		// Load Facebook API if necessary
		if (!this.FB) {
			return facebookLoader.load(() => {
				this.onLoginClick();
			});
		}


		this.parent.classList.add('fundraising-login--is-busy');

		// Facebook login
		this.FB.getLoginStatus((response) => {
			if (response.status == 'connected') {
				this.api.facebookLogin(response.authResponse.accessToken, this.onReponse.bind(this));
			} else {
				this.FB.login((response) => {
					if (response.status == 'connected') {
						this.api.facebookLogin(response.authResponse.accessToken, this.onReponse.bind(this));
					} else {
						alert('Error: failed to connect to Facebook');
						this.form.classList.remove('fundraising-login--is-busy');
					}
				}, {scope: 'email'});
			}
		});

	}

	onLoginSubmit(e) {
		e.preventDefault();
		this.parent.classList.add('fundraising-login--is-busy');

		let data = {
			email:    e.target.querySelector('input[name=email]')    .value,
			password: e.target.querySelector('input[name=password]') .value,
		};

		this.api.login(data, this.onReponse.bind(this));
	}

	onLogoutClick() {
		this.api.logout(() => location.reload());
	}

	onReponse(data) {
		if (data.error) {
			this.parent.classList.remove('fundraising-login--is-busy');
			alert(`An error occurred: ${data.msg}`);
			return;
		}

		if (data.existing_user)
			location.href = `/fundraising/profile/${data.session_profile_id}`;
		else
			location.href = `/fundraising/profile/${data.profile_id}#welcome`;
	}
};
