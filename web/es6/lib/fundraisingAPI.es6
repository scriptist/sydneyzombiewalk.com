'use strict';

const ZombieAPI = require('lib/ZombieAPI.es6');

class FundraisingAPI {
	constructor() {
		// Init Zombie API
		this._handleResponse = this._handleResponse.bind(this);
		this._checkSessionAge = this._checkSessionAge.bind(this);
		this.login = this.login.bind(this);
		this.facebookLogin = this.facebookLogin.bind(this);
		this.logout = this.logout.bind(this);
		this.editProfile = this.editProfile.bind(this);
		this.registerDonation = this.registerDonation.bind(this);
		this.profile = this.profile.bind(this);
		this.top = this.top.bind(this);
		this.sessionInfo = this.sessionInfo.bind(this);
		this.api = new ZombieAPI();
		this.sessionExpiry = 5 * 60 * 1000;

		this._restoreSession();

		setInterval(this._checkSessionAge, 10000);
	}

	_makeCallback(callback) {
		return (data) => {
			this._handleResponse(data);
			typeof callback === 'function' && callback(data);
		};
	}

	_handleResponse(data) {
		if ('session_profile_id' in data) {
			this.session = {
				profileId   : data.session_profile_id,
				loggedIn    : !!data.session_profile_id,
				lastUpdated : (new Date()).getTime(),
			};
			this._saveSession();
		}
	}

	_restoreSession() {
		try {
			return this.session = JSON.parse(localStorage['fundraising-session']);
		} catch (error) {
			// Do nothing
		}

		if (!this.session || this.session.lastUpdated < (new Date()).getTime() - this.sessionExpiry)
			this.session = {loggedIn: false};
	}


	_saveSession() {
		try {
			return localStorage['fundraising-session'] = JSON.stringify(this.session);
		} catch (error) {
			// Do nothing
		}
	}

	_checkSessionAge() {
		// Update session to check if logged out
		if (this.session.loggedIn && this.session.lastUpdated < (new Date()).getTime() - this.sessionExpiry)
			this.sessionInfo();
	}

	login(data, callback) {
		return this.api.post('/fundraising/session/login', data, this._makeCallback(callback));
	}

	facebookLogin(accessToken, callback) {
		return this.api.post('/fundraising/session/login-facebook', {accessToken: accessToken}, this._makeCallback(callback));
	}

	logout(callback) {
		return this.api.post('/fundraising/session/logout', {}, this._makeCallback(callback));
	}

	editProfile(id, data, callback) {
		return this.api.post(`/fundraising/profile/${id}`, data, this._makeCallback(callback));
	}

	registerDonation(id, data, callback) {
		return this.api.post(`/fundraising/profile/${id}/register_donation`, data, this._makeCallback(callback));
	}

	profile(id, callback) {
		return this.api.get(`/fundraising/profile/${id}`, this._makeCallback(callback));
	}

	top(count, callback) {
		return this.api.get(`/fundraising/stats/top/${count}`, this._makeCallback(callback));
	}

	sessionInfo(callback) {
		return this.api.get('/fundraising/session/info', this._makeCallback(callback));
	}
}

let fundraisingAPI;
module.exports = () => {
	fundraisingAPI = fundraisingAPI || new FundraisingAPI({
		appId: '330484023650533',
		xfbml: false,
		version: 'v2.4',
	});

	return fundraisingAPI;
};
