'use strict';

module.exports = class ZombieAPI {
	constructor() {
		if (window.szw_settings.develop) {
			this.baseURL = '//localhost.api.sydneyzombiewalk.com/v1';
		} else {
			this.baseURL = '//api.sydneyzombiewalk.com/v1';
		}
	}

	get(path, callback) {
		// Add cachebreaker
		var d = new Date();
		var t = d.getTime();
		if (path.indexOf('?') === -1) {
			path += `?t=${t}`;
		} else {
			path += `&t=${t}`;
		}

		return this._sendRequest('GET', path, null, callback);
	}

	post(path, data, callback) {
		return this._sendRequest('POST', path, data, callback);
	}

	_sendRequest(method, path, data, callback) {
		var url = `${this.baseURL}${path}`;

		var useXDom = !('withCredentials' in new XMLHttpRequest());
		if (useXDom) {
			var xhr = new XDomainRequest();
		} else {
			xhr = new XMLHttpRequest();
		}

		xhr.open(method, url, true);
		xhr.withCredentials = true;

		var onLoad = function() {
			try {
				var response = useXDom ? xhr.responseText : xhr.response;
				response = JSON.parse(response);
			} catch (error) {
				alert(`Error calling API ${path}: invalid response received.`);
			}

			if (typeof callback === 'function') {
				return callback(response);
			}
		};

		if (useXDom) {
			xhr.onload = onLoad;
		} else {
			xhr.addEventListener('load', onLoad);
		}

		if (method === 'POST') {
			if (!useXDom) {
				xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
			}
			return xhr.send(JSON.stringify(data));
		} else {
			return xhr.send();
		}
	}
};
