'use strict';

const apiKey = 'AIzaSyDetWeCZO5eOXy0k_0kWiQBpNRQC7_nsI8';
const routeArr = [
	[-33.880643, 151.207768],
	[-33.880135, 151.207500],
	[-33.880010, 151.206931],
	[-33.879369, 151.207092],
	[-33.876661, 151.207817],
	[-33.874441, 151.208001],
	[-33.874534, 151.209024],
	// [-33.873138, 151.208138],
	// [-33.873266, 151.209179],
	[-33.878290, 151.208578],
	[-33.880401, 151.208342],
	[-33.880570, 151.208096],
];
const style = [{'stylers':[{'saturation':-65}, {'visibility':'off'}]}, {'featureType':'road', 'stylers':[{'visibility':'on'}]}, {'featureType':'water', 'stylers':[{'visibility':'simplified'}]}, {'featureType':'poi.park', 'stylers':[{'visibility':'simplified'}]}, {'featureType':'transit.station.rail', 'stylers':[{'visibility':'on'}]}, {'featureType':'administrative', 'stylers':[{'visibility':'on'}]}];

module.exports = class RouteMap {
	constructor(parent) {
		this.parent = parent;
		if ('google' in window && 'maps' in window.google) {
			this.drawMap();
		} else {
			this.loadGoogleMaps(this.drawMap);
		}
	}

	loadGoogleMaps(callback) {
		let functionName = null;
		while (functionName in window || !functionName) {
			functionName = 'routeMap' + Math.floor(Math.random() * 1000000);
		}
		window[functionName] = () => {
			this.google = window.google;
			callback.apply(this);
		};

		const script = document.createElement('script');
		script.src = `https://maps.googleapis.com/maps/api/js?callback=${functionName}&key=${apiKey}`;

		const firstScript = document.getElementsByTagName('script')[0];
		firstScript.parentNode.insertBefore(script, firstScript);
	}

	drawMap() {
		const route = [];
		const bounds = new this.google.maps.LatLngBounds();

		for (let i = 0, coords; i < routeArr.length; i++) {
			coords = routeArr[i];
			const latLng = new this.google.maps.LatLng(coords[0], coords[1]);
			route.push(latLng);
			bounds.extend(latLng);
		}

		const lineSymbol = {path: this.google.maps.SymbolPath.FORWARD_CLOSED_ARROW};

		const polylineOptions = {
			path: route,
			strokeColor: '#3344dd',
			strokeWeight: 3,
			icons: [
				{
					icon: lineSymbol,
					offset: '20%',
				},
				{
					icon: lineSymbol,
					offset: '53%',
				},
				{
					icon: lineSymbol,
					offset: '80%',
				},
			],
		};

		const mapOptions = {
			draggable: window.innerWidth > 640,
			mapTypeControl: false,
			scrollWheel: false,
			streetViewControl: false,
			styles: style,
		};

		const polyline = new this.google.maps.Polyline(polylineOptions);
		const map = new this.google.maps.Map(this.parent, mapOptions);
		polyline.setMap(map);
		map.fitBounds(bounds);
	}
};
