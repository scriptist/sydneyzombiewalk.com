'use strict';

const months = [
	'Jan',
	'Feb',
	'Mar',
	'Apr',
	'May',
	'Jun',
	'Jul',
	'Aug',
	'Sep',
	'Oct',
	'Nov',
	'Dec',
];

let utils = module.exports = {
	currencyFormat: function (amt, cents) {
		var decimalPlaces;
		amt = parseFloat(amt);
		if (isNaN(amt))
			return null;
		if (cents !== true && cents !== false)
			cents = amt !== parseInt(amt);
		decimalPlaces = cents ? 2 : 0;
		return '$' + amt.toFixed(decimalPlaces);
	},
	dateFormat: function (dateStr, includeTime, tzOffset) {
		var date, dateString, h, timeString;
		if (null == includeTime)
			includeTime = false;
		if (null == tzOffset)
			tzOffset = 11;
		if (dateStr instanceof Date) {
			date = dateStr;
		} else {
			date = new Date(dateStr);
			if (isNaN(date.getTime())) {
				dateStr = dateStr.replace(' ', 'T');
				date = new Date(dateStr);
			}
		}
		h = date.getUTCHours() + tzOffset;
		timeString = (h <= 12 ? h : h % 12) + ':' + utils.padNumber(date.getUTCMinutes(), 2) + (h < 12 ? 'am' : 'pm');
		dateString = months[date.getUTCMonth()] + ' ' + date.getUTCDate() + ', ' + date.getUTCFullYear();
		if (includeTime) {
			return timeString + ' ' + dateString;
		} else {
			return dateString;
		}
	},
	padNumber: function (num, length) {
		var s;
		s = num.toString();
		while (s.length < length) {
			s = '0' + s;
		}
		return s;
	},
};
