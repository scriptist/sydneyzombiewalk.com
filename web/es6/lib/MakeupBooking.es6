'use strict';

const Vue = require('Vue');
const ZombieAPI = require('lib/ZombieAPI.es6');
const utils = require('lib/utils.es6');

class MakeupBooking {
	constructor(parent) {
		this.parent = parent;
		this.dateStr = this.parent.getAttribute('data-date');
		this.date = new Date(this.dateStr);
		this.date.setHours(0);
		this.api = new ZombieAPI;
		this.getSlots();

		this.busy = true;
		this.href = location.href;

		this.selectedArtist = null;
		this.selectedHour = null;
		this.selectedSlot = null;
		this.customerName = null;
		this.customerEmail = null;
		this.bookingId = null;

		this.vue = new Vue({
			el: this.parent,
			data: this,
			methods: {
				selectArtist: this.selectArtist.bind(this),
				submitDetails: this.submitDetails.bind(this),
			},
		});
	}

	selectArtist(artist, hour) {
		this.selectedArtist = artist;
		this.selectedHour = hour;
	}

	submitDetails() {
		this.busy = true;
		this.api.post(`/makeup/slot/${this.selectedSlot.id}/register`, {
			name: this.customerName,
			email: this.customerEmail,
		}, (result) => {
			this.busy = false;
			if (result.error) {
				alert(result.msg);
			} else {
				this.bookingId = result.id;
				Vue.nextTick(() => {
					this.parent.querySelector('#paypal-form').submit();
				});
			}
		});
	}

	getSlots(cb) {
		this.artists = null;
		this.hours = [];
		this.api.get(`/makeup/slots/${this.dateStr}`, (result) => {
			if (result.error) {
				this.busy = false;
				return alert(result.error);
			}

			this.artists = result.artists.map((artist) => {
				return new MakeupArtist(artist);
			});

			let cursor = new Date(this.date.getTime());
			cursor.setHours(11);

			this.artists.forEach((artist) => {
				if (!artist.slots.length)
					return;

				if (artist.slots[0].time.getHours() < cursor.getHours())
					cursor.setHours(artist.slots[0].time.getHours());
			});

			while (cursor.getHours() < 16) {
				this.hours.push(new Date(cursor.getTime()));
				cursor.setHours(cursor.getHours() + 1);
			}

			this.busy = false;

			typeof cb === 'function' && cb();
		});
	}
}

class MakeupArtist {
	constructor(data) {
		this.id = parseInt(data.id);
		this.name = data.name;
		this.price = parseFloat(data.price);
		this.slots = data.slots.map((slot) => {
			return new MakeupSlot(slot);
		});
	}

	getSlots(hour) {
		if (hour === 'undefined')
			return this.slots;

		let start = new Date(hour);
		let end = new Date(hour);
		end.setHours(end.getHours() + 1);

		return this.slots.filter((slot) => {
			return slot.time >= start && slot.time < end;
		});
	}
}

class MakeupSlot {
	constructor(data) {
		this.id = parseInt(data.id);
		this.artistId = parseInt(data.artist_id);
		this.time = new Date(data.datetime);
	}

	get timeStr() {
		let s = '';

		s += this.time.getHours() > 12 ? this.time.getHours() - 12 : this.time.getHours();
		s += ':' + utils.padNumber(this.time.getMinutes(), 2);
		s += ' ' + (this.time.getHours() > 11 ? 'pm' : 'am');

		return s;
	}
}

module.exports = MakeupBooking;
