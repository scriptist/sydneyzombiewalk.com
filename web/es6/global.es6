'use strict';

require('lib/modernizr.custom.js');

require('lib/nav.es6')();

const init = [
	{
		selector: '.fundraising-login',
		class: require('lib/FundraisingLogin.es6'),
	},
	{
		selector: '.profile',
		class: require('lib/FundraisingProfile.es6'),
	},
	{
		selector: '.fundraising-top',
		class: require('lib/FundraisingTop.es6'),
	},
	{
		selector: '.route-map',
		class: require('lib/RouteMap.es6'),
	},
	{
		selector: 'form.register',
		class: require('lib/RegisterForm.es6'),
	},
	{
		selector: '.ticket',
		class: require('lib/Ticket.es6'),
	},
];

for (let i = 0; i < init.length; i++) {
	const elms = document.querySelectorAll(init[i].selector);
	for (let j = 0; j < elms.length; j++) {
		new init[i].class(elms[j]);
	}
}
